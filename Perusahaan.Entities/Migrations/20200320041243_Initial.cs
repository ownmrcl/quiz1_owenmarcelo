﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Perusahaan.Entities.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "perusahaan",
                columns: table => new
                {
                    PerusahaanID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NamaPerusahaan = table.Column<string>(nullable: false),
                    JumlahPerusahaan = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_perusahaan", x => x.PerusahaanID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "perusahaan");
        }
    }
}
