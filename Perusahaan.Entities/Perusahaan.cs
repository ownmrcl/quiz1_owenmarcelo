﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Perusahaan.Entities
{
    public class DataPerusahaan
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PerusahaanID { get; set; }
        [Required]
        public string NamaPerusahaan { get; set; }
        [Required]
        public int JumlahPerusahaan { get; set; }
    }
}