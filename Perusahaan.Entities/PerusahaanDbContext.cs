﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Perusahaan.Entities
{
   public class PerusahaanDbContext:DbContext
    {
        public PerusahaanDbContext(DbContextOptions<PerusahaanDbContext> options):base(options)
        {

        }

        public DbSet<DataPerusahaan> Perusahaan { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
            modelBuilder.Entity<DataPerusahaan>().ToTable("perusahaan");
        }
    }
}
