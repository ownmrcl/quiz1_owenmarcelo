﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Perusahaan.Entities
{
    public class PerusahaanDbContextFactory : IDesignTimeDbContextFactory<PerusahaanDbContext>
    {
        public PerusahaanDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PerusahaanDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new PerusahaanDbContext(builder.Options);
        }
    }
}
