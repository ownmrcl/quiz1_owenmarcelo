﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quiz1.Models;
using Quiz1.Services;

namespace Quiz1.API
{
    [Route("api/v1/perusahaan")]
    [ApiController]
    public class PerusahaanApiController : ControllerBase
    {
        private readonly PerusahaanService ServiceMan;

        public PerusahaanApiController(PerusahaanService perusahaanService)
        {
            this.ServiceMan = perusahaanService;
        }

        // GET: Mengakses semua data perusahaan
        [HttpGet("all-perusahaan", Name = "allPerusahaan")]
        public async Task<ActionResult<List<PerusahaanModel>>> GetAllPerusahaanAsync()
        {
            var perusahaan = await this.ServiceMan.GetAllPerusahaan();
            return Ok(perusahaan);
        }

        // POST: Insert data baru ke dalam table
        [HttpPost("insert-perusahaan", Name = "insertPerusahaan")]
        public async Task<ActionResult<ResponseModel>> InsertPerusahaanAsync([FromBody]PerusahaanModel value)
        {
            var isSuccess = await this.ServiceMan.InsertPerusahaan(value);
            return Ok(new ResponseModel
            {
                ResponseMessage = "Successfully insert new data"
            });
        }

        [HttpPost("insert-new-perusahaan", Name = "insertNewPerusahaan")]
        public async Task<ActionResult<ResponseModel>> InsertNewPerusahaan(int id, [FromQuery]string nama, [FromQuery]int jumlah )
        {
            var isSuccess = await this.ServiceMan.InsertNewPerusahaan(id, nama, jumlah);
            
            if(isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success"
            });
        }

        // PUT: Update data yang sudah ada di dalam table
        [HttpPut("update-perusahaan", Name = "updatePerusahaan")]
        public async Task<ActionResult<ResponseModel>> UpdatePerusahaan([FromBody]PerusahaanModel value)
        {
            var isSuccess = await ServiceMan.UpdatePerusahaanAsync(value);
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Successfully update {value.NamaPerusahaan} data"
            });

        }

        
        // DELETE: Delete data yang sudah ada di dalam table
        [HttpDelete("delete-perusahaan", Name = "deletePerusahaan")]
        public async Task<ActionResult<ResponseModel>> DeletePerusahaan([FromBody]PerusahaanModel value)
        {
            var isSuccess = await ServiceMan.DeletePerusahaanAsync(value.PerusahaanID);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Successfully delete {value.NamaPerusahaan} data"
            });
        }

        // GET: Mengakses data perusahaan dengan spesifik sesuai dengan ID yang dipilih
        [HttpGet("get-specific-data", Name = "getSpecificData")]
        public async Task<ActionResult<PerusahaanModel>> GetSpecificData(string perusahaanID)
        {
            if (string.IsNullOrEmpty(perusahaanID) == false)
            {
                return BadRequest(null);
            }

            var id = Int32.Parse(perusahaanID);
            var perusahaan = await ServiceMan.GetSpesificPerusahaanAsync(id);
            if (perusahaan == null)
            {
                return BadRequest(null);
            }
            return Ok(perusahaan);
        }

        [HttpGet("filter-data", Name = "filterData")]
        public async Task<ActionResult<List<PerusahaanModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await ServiceMan.GetAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = ServiceMan.GetTotalData();

            return Ok(totalData);
        }
    }
}