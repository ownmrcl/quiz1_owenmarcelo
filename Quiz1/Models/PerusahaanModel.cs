﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1.Models
{
    public class PerusahaanModel
    {
        public int PerusahaanID { get; set; }
        [Required]
        [StringLength(7, MinimumLength = 3)]
        [Display(Name = "Nama")]
        public string NamaPerusahaan { get; set; }
        [Required]
        public int JumlahPerusahaan { get; set; }
    }
}
