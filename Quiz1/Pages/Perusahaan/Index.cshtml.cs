﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Quiz1.Models;
using Quiz1.Services;

namespace Quiz1
{
    public class IndexModel : PageModel
    {
        private readonly PerusahaanService ServiceMan;
        public IndexModel(PerusahaanService perusahaanService)
        {
            this.ServiceMan = perusahaanService;
        }
        [BindProperty]
        public List<PerusahaanModel> Perusahaan { get; set; }

        // Data filtering
        //[Display(Name = "Search")]
        //[BindProperty(SupportsGet = true)]
        //public string FilterByname { get; set; }
        //public int TotalPage { get; set; }
        //[BindProperty(SupportsGet = true)]
        //public int PageIndex { get; set; }
        //public int ItemPerPage => 3; 

        //public async Task OnGetAsync()
        //{
        //    if(PageIndex == 0)
        //    {
        //        PageIndex = 1;
        //    }

        //    Perusahaan = await ServiceMan.GetAsync(PageIndex, ItemPerPage, FilterByname);

        //    var totalPerusahaan = ServiceMan.GetTotalData();

        //    TotalPage = (int)Math.Ceiling(totalPerusahaan / (double)ItemPerPage);
        //}

        // Dibuat agar bisa redirect kembali ke url /perusahaan setelah melakukan delete
        public async Task<IActionResult> OnPostDelete(int id)
        {
            var isSuccess = await this.ServiceMan.DeletePerusahaanAsync(id);

            if(isSuccess == false)
            {
                return Page();
            }
            return Redirect("/perusahaan");
        }
    }
}