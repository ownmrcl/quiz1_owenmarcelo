﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Quiz1.Models;
using Quiz1.Services;

namespace Quiz1
{
    public class UpdateModel : PageModel
    {
        private readonly PerusahaanService ServiceMan;
        public UpdateModel(PerusahaanService perusahaanService)
        {
            this.ServiceMan = perusahaanService;
        }

        [BindProperty]
        public PerusahaanModel CurrentPerusahaan { set; get; }

        public async Task OnGetAsync(int id)
        {
            CurrentPerusahaan = await ServiceMan.GetSpesificPerusahaanAsync(id);
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var isExist = await ServiceMan.UpdatePerusahaanAsync(CurrentPerusahaan);

            if(isExist == false)
            {
                return Page();
            }
            return Redirect("/perusahaan");
        }


    }
}