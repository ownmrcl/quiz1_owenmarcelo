﻿using Perusahaan.Entities;
using Quiz1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1.Services
{
    public class PerusahaanService
    {
        private readonly PerusahaanDbContext _db;

        public PerusahaanService(PerusahaanDbContext dbContext)
        {
            this._db = dbContext;
        }

        /// <summary>
        /// Digunakan untuk menambahkan data perusahaan baru
        /// </summary>
        public async Task<bool> InsertPerusahaan(PerusahaanModel perusahaanModel)
        {
            this._db.Add(new DataPerusahaan
            {
                PerusahaanID = perusahaanModel.PerusahaanID,
                NamaPerusahaan = perusahaanModel.NamaPerusahaan,
                JumlahPerusahaan = perusahaanModel.JumlahPerusahaan
            });

            await this._db.SaveChangesAsync();
            return true;
        }

        public async Task<bool> InsertNewPerusahaan(int id, string nama, int jumlah)
        {
            this._db.Add(new DataPerusahaan
            {
                PerusahaanID = id,
                NamaPerusahaan = nama,
                JumlahPerusahaan = jumlah
            });
            await this._db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Mencari semua data di dalam table perusahaan
        /// </summary>
        public async Task<List<PerusahaanModel>> GetAllPerusahaan()
        {
            var data = await this._db
                .Perusahaan
                .Select(Q => new PerusahaanModel
                {
                    PerusahaanID = Q.PerusahaanID,
                    NamaPerusahaan = Q.NamaPerusahaan,
                    JumlahPerusahaan = Q.JumlahPerusahaan
                })
                .AsNoTracking()
                .ToListAsync();

            return data;
        }

        /// <summary>
        /// Mencari data perusahaan berdasarkan ID yang terpilih
        /// </summary>
        public async Task<PerusahaanModel> GetSpesificPerusahaanAsync(int perusahaanID)
        {
            var perusahaan = await this._db
                .Perusahaan
                .Where(Q => Q.PerusahaanID == perusahaanID)
                .Select(Q => new PerusahaanModel
                {
                    NamaPerusahaan = Q.NamaPerusahaan,
                    JumlahPerusahaan = Q.JumlahPerusahaan
                })
                .FirstOrDefaultAsync();

            return perusahaan;
        }

        /// <summary>
        /// Update data perusahaan yang dipilih
        /// </summary>
        public async Task<bool> UpdatePerusahaanAsync(PerusahaanModel perusahaan)
        {
            var perusahaanModel = await this._db
                .Perusahaan
                .Where(Q => Q.PerusahaanID == perusahaan.PerusahaanID)
                .FirstOrDefaultAsync();

            if (perusahaanModel == null)
            {
                return false;
            }

            perusahaanModel.NamaPerusahaan = perusahaan.NamaPerusahaan;
            perusahaanModel.JumlahPerusahaan = perusahaan.JumlahPerusahaan;

            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Delete perusahaan yang terpilih
        /// </summary>
        public async Task<bool> DeletePerusahaanAsync(int perusahaanID)
        {
            var perusahaanModel = await this._db
                .Perusahaan
                .Where(Q => Q.PerusahaanID == perusahaanID)
                .FirstOrDefaultAsync();

            if (perusahaanModel == null)
            {
                return false;
            }

            this._db.Remove(perusahaanModel);
            await this._db.SaveChangesAsync();

            return true;
        }
       
        /// <summary>
        /// Filtering data
        /// </summary>
        public async Task<List<PerusahaanModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            var query = this._db
                .Perusahaan
                .AsQueryable();

            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.NamaPerusahaan.Contains(filterByName));
            }

            var perusahaan = await query
                .Select(Q => new PerusahaanModel
                {
                    PerusahaanID = Q.PerusahaanID,
                    NamaPerusahaan = Q.NamaPerusahaan,
                    JumlahPerusahaan = Q.JumlahPerusahaan
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return perusahaan;
        }

    public int GetTotalData()
        {
            var totalPerusahaan = this._db
                .Perusahaan
                .Count();

            return totalPerusahaan;
        }
    }
}
