﻿export interface PerusahaanModelTemp {
    perusahaanID?: string;
    namaPerusahaan?: string;
    jumlahPerusahaan?: number;
}